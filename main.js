
function showError(e) {
  addToLog(e.toString());
  alert('Something went wrong');
}

function addToLog(text) {
  console.log(text);
  logView.value = text + '\n' + logView.value;
}

function onLanguageChange(language) {
  btnEnglish.classList.toggle('selected', language === 'English');
  btnNorwegian.classList.toggle('selected', language === 'Norwegian');
}

function onVolumeChange(volume) {
  addToLog('volume ' + volume);
  sliderVolume.value = parseInt(volume);
}

function onApiReady() {
  addToLog('ready!');
  getLanguage().then(onLanguageChange).catch(showError);
  getVolume().then(onVolumeChange).catch(showError);
}

function reload() {
  location.reload();
}

function call() {
  const uri = inputUri.value.trim();
  if (uri) {
    xapi.command('Dial', { Number: uri });
  }
  else {
    alert('Enter a number');
  }
}

function onWebAppInstall() {
  const id = inputWebAppId.value;
  const name = inputWebAppName.value;
  const url = inputWebAppUrl.value;
  if (id, name, url) {
    installWebApp({ id, name, url });
  }
  else {
    alert('You must provide id, name and url');
  }
}

function onWebAppOpen() {
  const url = inputWebAppUrl.value;
  if (url) openWebUrl(url);
  else alert('You must provide a URL')
}

function onWebAppDelete() {
  const id = inputWebAppId.value;
  if (id) removeWebApp(id);
  else alert('You must provide an id');
}

function onVolumeSliderChanged() {
  const level = sliderVolume.value;
  setVolume(level);
  addToLog('vol ' + val);
}

function init() {
  initXapi(onApiReady);
  btnToggleMute.onclick = toggleMute;
  btnRefresh.onclick = reload;
  btnClose.onclick = closeWebview;
  btnNorwegian.onclick = () => setLanguage('Norwegian');
  btnEnglish.onclick = () => setLanguage('English');
  btnCall.onclick = call;
  btnWebAppInstall.onclick = onWebAppInstall;
  btnWebAppOpen.onclick = onWebAppOpen;
  btnWebAppDelete.onclick = onWebAppDelete;
  sliderVolume.onchange = onVolumeSliderChanged;
}

window.onload = init;
