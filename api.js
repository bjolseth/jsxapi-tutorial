let xapi;

function initXapi(onReady) {
  if (window.getXAPI) {
    window.getXAPI().then((r) => {
      xapi = r;
    });
  }
  else {
    xapi = mockXapi;
  }

  // timer shouldnt be necessary, but currently is, half of the time
  setTimeout(onReady, 500);
}

function mock(...args) {
  const log = args.map(a => JSON.stringify(a)).join('/');
  addToLog(log);
  return Promise.resolve(true);
}

const mockXapi = {
  command: (cmd, values, multiline) => mock('command', cmd, values, multiline),
  config: {
    get: (config) => mock('config get', config),
    set: (config, value) => mock('config set', config, value),
    on: (path) => mock('config subscribe', path),
  },
  status: {
    get: (path) => mock('status get', path, null, 50),
    on: (path) => mock('status subscribe', path)
  },
  event: {
    on: (path) => mock('event subscribe', path),
  },
};


function toggleMute() {
  // xapi.Command.Audio.Volume.ToggleMute()
  return xapi.command('Audio Volume ToggleMute')
}

function setVolume(Level) {
  return xapi.command('Audio Volume Set', { Level });
}

function getVolume() {
  return xapi.status.get('Audio Volume');
}

function setLanguage(language) {
  return xapi.config.set('UserInterface Language', language);
}

function getLanguage() {
  return xapi.config.get('UserInterface Language')
}

function closeWebview() {
  return xapi.command('UserInterface WebView Clear');
}

const panelXml = `
<Extensions>
  <Version>1.6</Version>
  <Panel>
    <PanelId>$id</PanelId>
    <Type>Home</Type>
    <Order>-1</Order>
    <Name>$name</Name>
    <ActivityType>WebApp</ActivityType>
    <ActivityData>$url</ActivityData>
  </Panel>
</Extensions>
`;

function installWebApp(app) {
  const xml = panelXml
    .replace('$id', app.id)
    .replace('$name', app.name)
    .replace('$url', app.url);

  return xapi.command('UserInterface Extensions Panel Save', { PanelId: app.id }, xml);
}

function removeWebApp(id) {
  return xapi.command('UserInterface Extensions Panel Remove', { PanelId: id });
}

function openWebUrl(Url) {
  return xapi.command('UserInterface WebView Display', { Url });
}

function getApps() {
  return xapi.command('UserInterface Extensions List');
}

function showSignage() {
  return xapi.command('Standby Halfwake');
}

function setSignage(url) {
  xapi.config.set('Standby Signage Mode', 'On');
  return xapi.config.set('Standby Signage Url', url);
}
