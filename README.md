# JSXapi Tutorial

This project contains a simple web page that communicates with Cisco Video codecs using the new built-in xapi adapter.

Requirements:
- A video device that supports web views (Room Kits, Webex Board, Desk Pro, ...)
- Registered on an org with feature toggle for xapi integration
- xapi config enabled (xConfig WebEngine Features AllowXAPI: On)


## Status

Currently working / not working:

✅ xcommand
✅ xstatus get
✅ xconfig get/set
✅ xcommand Multi-line parameters

❌ xstatus feedback
❌ xconfig feedback
❌ xevent feedback
